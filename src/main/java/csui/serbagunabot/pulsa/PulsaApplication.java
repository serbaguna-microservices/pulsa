package csui.serbagunabot.pulsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulsaApplication {
    public static void main(String[] args) {
        SpringApplication.run(PulsaApplication.class, args);
    }
}
